﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace OA.Data
{
    public class Book : BaseEntity
    {
        [Required]
        public string? Title { get; set; }
        public string? Language { get; set; }
        [NotMapped]
        public IFormFile? BookImage { get; set; }
        public string? BookImageUrl { get; set; }
        public int AuthorId { get; set; }

        public virtual Author? Author { get; set; }
    }
}
