﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Data
{
    public class AuthorMap
    {
        public AuthorMap(EntityTypeBuilder<Author> entityBuilder)
        {
            entityBuilder.HasKey(a => a.Id);
            entityBuilder.Property(a => a.Name);
            entityBuilder.Property(a => a.Address);
            entityBuilder.Ignore(a => a.AuthorImage);
            entityBuilder.Property(a => a.AuthorImageUrl);
            entityBuilder.HasMany(a => a.Books).WithOne(b => b.Author).HasForeignKey(b => b.AuthorId);
        }
    }
}
