﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Data
{
    public class BookMap
    {
        public BookMap(EntityTypeBuilder<Book> entityBuilder)
        {
            entityBuilder.HasKey(b => b.Id);
            entityBuilder.Property(b => b.Title).IsRequired();
            entityBuilder.Property(b => b.Language);
            entityBuilder.Ignore(b => b.BookImage);
            entityBuilder.Property(b => b.BookImageUrl);
            entityBuilder.Property(b => b.AuthorId).IsRequired();
            entityBuilder.HasOne(b => b.Author).WithMany().HasForeignKey(b => b.AuthorId);
        }
    }
}
