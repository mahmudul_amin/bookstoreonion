﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Data
{
    public class Author : BaseEntity
    {
        public string? Name { get; set; }
        public string? Address { get; set; }
        [NotMapped]
        public IFormFile? AuthorImage { get; set; }
        public string? AuthorImageUrl { get; set; }

        public virtual ICollection<Book>? Books { get; set; }
    }
}
