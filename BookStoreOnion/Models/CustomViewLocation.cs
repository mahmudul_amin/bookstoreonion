﻿using Microsoft.AspNetCore.Mvc.Razor;

public class CustomViewLocation : IViewLocationExpander
{
    public void PopulateValues(ViewLocationExpanderContext context)
    {
    }

    public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
    {

        var customVeiewLocation = new[]
        {
            "/Minar/Book/{1}/{0}.cshtml"
        };

        viewLocations = customVeiewLocation.Concat(viewLocations);

        return viewLocations;
    }
}