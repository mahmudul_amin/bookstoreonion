﻿using Microsoft.AspNetCore.Mvc;
using OA.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreOnion.Models
{
    public class AuthorViewModel
    {
        [HiddenInput]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        [NotMapped]
        public IFormFile? AuthorImage { get; set; }
        public string? AuthorImageUrl { get; set; }

        public virtual ICollection<Book>? Books { get; set; }
    }
}
