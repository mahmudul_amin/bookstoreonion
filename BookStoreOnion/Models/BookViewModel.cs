﻿using Microsoft.AspNetCore.Mvc;
using OA.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreOnion.Models
{
    public class BookViewModel
    {
        [HiddenInput]
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Language { get; set; }
        [NotMapped]
        public IFormFile? BookImage { get; set; }
        public string? BookImageUrl { get; set; }
        public int AuthorId { get; set; }

        public virtual Author? Author { get; set; }
    }
}
