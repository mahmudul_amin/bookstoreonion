﻿using BookStoreOnion.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OA.Data;
using OA.Service;

namespace BookStoreOnion.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IBookService bookService;
        private readonly IAuthorService authorService;
        private IWebHostEnvironment Environment;
        public AuthorController(IBookService bookService, IAuthorService authorService, IWebHostEnvironment _environment)
        {
            this.bookService = bookService;
            this.authorService = authorService;
            Environment = _environment;
        }
        public IActionResult Index()
        {
            List<AuthorViewModel> model = new List<AuthorViewModel>();
            authorService.GetAuthors().ToList().ForEach(a =>
            {
                Author authorDetails = authorService.GetAuthor(a.Id);
                AuthorViewModel author = new AuthorViewModel
                {
                    Id = a.Id,
                    Name = a.Name,
                    Address = a.Address,
                    AuthorImageUrl = a.AuthorImageUrl,
                };
                model.Add(author);
            });
            return View(model);
        }


        // GET: Author/Details/5
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var getId = id;

            var a = authorService.GetAuthor(id);
            var books = bookService.GetBooksByAuthor(id);

            AuthorViewModel author = new AuthorViewModel
            {
                Id = a.Id,
                Name = a.Name,
                Address = a.Address,
                AuthorImageUrl = a.AuthorImageUrl,
                Books = books.ToList(),
            };
            //ViewBag.books = books;

            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }


        // GET: Author/Create
        public IActionResult Create()
        {
            var model = new AuthorViewModel();
            return View(model);
        }


        // POST: Author/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AuthorViewModel model)
        {
            //var author = authorService.GetAuthor(model.Id);

            if (ModelState.IsValid)
            {
                var new_author = new Author
                {
                    Name = model.Name,
                    Address = model.Address,
                    AuthorImageUrl = model.AuthorImageUrl,
                };

                if (model.AuthorImage != null && model.AuthorImage.Length > 0)
                {
                    var date = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var fileName = date + Path.GetFileName(model.AuthorImage.FileName);
                    var filePath = Path.Combine(Environment.WebRootPath, "images", fileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        model.AuthorImage.CopyTo(fileStream);
                    }
                    new_author.AuthorImageUrl = "/images/" + fileName;
                }

                authorService.InsertAuthor(new_author);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }


        // GET: Author/Edit/5
        public IActionResult Edit(int id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var author = authorService.GetAuthor(id);


            if (author == null)
            {
                return NotFound();
            }

            AuthorViewModel edited_author = new AuthorViewModel
            {
                Id = author.Id,
                Name = author.Name,
                Address = author.Address,
                AuthorImageUrl = author.AuthorImageUrl,
            };

            return View(edited_author);

            // set the Authors property to a SelectList containing all authors
            //return View(book);
        }


        // POST: Author/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, AuthorViewModel model)
        {

            if (id == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {

                var author = authorService.GetAuthor(id);


                if (author == null)
                {
                    return NotFound();
                }


                author.Id = model.Id;
                author.Name = model.Name;
                author.Address = model.Address;
                //author.AuthorImageUrl = model.BookImageUrl;

                authorService.UpdateAuthor(author);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }


        // GET: Author/Delete/5
        public IActionResult Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = authorService.GetAuthor(id);
            var books = bookService.GetBooksByAuthor(id);
            if (author == null)
            {
                return NotFound();
            }

            AuthorViewModel deleting_author = new AuthorViewModel
            {
                Id = author.Id,
                Name = author.Name,
                Address = author.Address,
                Books = books.ToList(),
            };

            return View(deleting_author);
        }

        // POST: Author/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (authorService.GetAuthors().ToList() == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Books'  is null.");
            }
            var author = authorService.GetAuthor(id);
            if (author != null)
            {
                authorService.DeleteAuthor(id);
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
