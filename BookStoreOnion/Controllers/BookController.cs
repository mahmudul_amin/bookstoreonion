﻿using BookStoreOnion.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using OA.Data;
using OA.Service;

namespace BookStoreOnion.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService bookService;
        private readonly IAuthorService authorService;
        private IWebHostEnvironment Environment;
        public BookController(IBookService bookService, IAuthorService authorService, IWebHostEnvironment _environment)
        {
            this.bookService = bookService;
            this.authorService = authorService;
            Environment = _environment;
        }



        // GET: Books
        [HttpGet]
        public IActionResult Index()
        {
            List<BookViewModel> model = new List<BookViewModel>();
            bookService.GetBooks().ToList().ForEach(b =>
            {
                Book bookDetails = bookService.GetBook(b.Id);
                BookViewModel book = new BookViewModel
                {
                    Id = b.Id,
                    Title = b.Title,
                    Language = b.Language,
                    BookImageUrl = b.BookImageUrl,
                    Author = authorService.GetAuthor(b.AuthorId),
                };
                model.Add(book);
            });
            return View(model);
        }


        // GET: Books/Details/5
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var b = bookService.GetBook(id);
            BookViewModel book = new BookViewModel
            {
                Id = b.Id,
                Title = b.Title,
                Language = b.Language,
                BookImageUrl = b.BookImageUrl,
                Author = authorService.GetAuthor(b.AuthorId),
            };
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Books/Create
        public IActionResult Create()
        {
            var authors = authorService.GetAuthors().ToList();


            var model = new BookViewModel();

            // set the Authors property to a SelectList containing all authors
            ViewBag.Authors = new SelectList(authors, "Id", "Name");

            return View(model);
        }




        // POST: Books/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BookViewModel model)
        {
            var author = authorService.GetAuthor(model.Id);

            if (ModelState.IsValid)
            {
                var new_book = new Book
                {
                    Title = model.Title,
                    Language = model.Language,
                    Author = model.Author,
                    AuthorId = model.AuthorId,
                };

                if (model.BookImage != null && model.BookImage.Length > 0)
                {
                    var date = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var fileName = date + Path.GetFileName(model.BookImage.FileName);
                    var filePath = Path.Combine(Environment.WebRootPath, "images", fileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        model.BookImage.CopyTo(fileStream);
                    }
                    new_book.BookImageUrl = "/images/" + fileName;
                }

                bookService.InsertBook(new_book);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Books/Edit/5
        public IActionResult Edit(int id)
        {
            var authors = authorService.GetAuthors().ToList();

            if (id == null)
            {
                return NotFound();
            }

            var book = bookService.GetBook(id);


            if (book == null)
            {
                return NotFound();
            }

            BookViewModel edited_book = new BookViewModel
            {
                Id = book.Id,
                Title = book.Title,
                Language = book.Language,
                BookImageUrl = book.BookImageUrl,
                Author = authorService.GetAuthor(book.AuthorId),
            };

            ViewBag.Authors = new SelectList(authors, "Id", "Name");
            return View(edited_book);

            // set the Authors property to a SelectList containing all authors
            //return View(book);
        }


        // POST: Books/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, BookViewModel model)
        {
            var authors = authorService.GetAuthors().ToList();

            if (id == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {

                var book = bookService.GetBook(id);


                if (book == null)
                {
                    return NotFound();
                }


                book.Id = model.Id;
                book.Title = model.Title;
                book.Language = model.Language;
                //book.BookImageUrl = model.BookImageUrl;
                book.Author = authorService.GetAuthor(model.AuthorId);

                bookService.UpdateBook(book);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Books/Delete/5
        public IActionResult Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book =  bookService.GetBook(id);
            if (book == null)
            {
                return NotFound();
            }

            BookViewModel deleting_book = new BookViewModel
            {
                Id = book.Id,
                Title = book.Title,
                Language = book.Language,
                BookImageUrl = book.BookImageUrl,
                Author = authorService.GetAuthor(book.AuthorId),
            };

            return View(deleting_book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id, BookViewModel model)
        {
            if (bookService.GetBooks().ToList() == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Books'  is null.");
            }
            var book =  bookService.GetBook(id);
            if (book != null)
            {
                bookService.DeleteBook(id);
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
