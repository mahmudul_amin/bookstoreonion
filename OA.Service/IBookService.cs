﻿using OA.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Service
{
    public interface IBookService
    {
        IEnumerable<Book> GetBooks();
        Book GetBook(int id);

        IEnumerable<Book> GetBooksByAuthor(int id);
        void InsertBook(Book book);
        void UpdateBook(Book book);
        void DeleteBook(int id);
    }
}
