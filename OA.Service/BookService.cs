﻿using OA.Data;
using OA.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Service
{
    public class BookService : IBookService
    {

        private IRepository<Book> bookRepository;
        //private IRepository<Author> authorRepository;

        public BookService(IRepository<Book> bookRepository, IRepository<Author> authorRepository)
        {
            this.bookRepository = bookRepository;
            //this.authorRepository = authorRepository;
        }

        public IEnumerable<Book> GetBooks()
        {
            return bookRepository.GetAll();
        }

        public IEnumerable<Book> GetBooksByAuthor(int authorId)
        {
            return bookRepository.GetAll().Where(b => b.AuthorId == authorId).ToList();
        }

        public Book GetBook(int id)
        {
            return bookRepository.Get(id);
        }

        public void InsertBook(Book book)
        {
            bookRepository.Insert(book);
        }

        public void UpdateBook(Book book)
        {
            bookRepository.Update(book);
        }

        public void DeleteBook(int id)
        {
            Book book = bookRepository.Get(id);
            bookRepository.Delete(book);
            bookRepository.SaveChanges();
        }
    }
}
