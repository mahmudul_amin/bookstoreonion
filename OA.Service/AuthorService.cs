﻿using OA.Data;
using OA.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA.Service
{
    public class AuthorService : IAuthorService
    {
        private IRepository<Book> bookRepository;
        private IRepository<Author> authorRepository;

        public AuthorService(IRepository<Book> bookRepository, IRepository<Author> authorRepository)
        {
            this.bookRepository = bookRepository;
            this.authorRepository = authorRepository;
        }

        public IEnumerable<Author> GetAuthors()
        {
            return authorRepository.GetAll();
        }

        public Author GetAuthor(int id)
        {
            return authorRepository.Get(id);
        }

        public void InsertAuthor(Author author)
        {
            authorRepository.Insert(author);
        }

        public void UpdateAuthor(Author author)
        {
            authorRepository.Update(author);
        }

        public void DeleteAuthor(int id)
        {
            Author author = authorRepository.Get(id);
            authorRepository.Delete(author);
            authorRepository.SaveChanges();
        }
    }
}
